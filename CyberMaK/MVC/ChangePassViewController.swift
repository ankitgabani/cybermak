//
//  ChangePassViewController.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBackk(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return .lightContent
       }
}
