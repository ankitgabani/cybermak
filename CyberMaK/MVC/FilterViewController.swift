//
//  FilterViewController.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var viewShort: UIView!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var btnShort: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewFilter.isHidden = false
        viewShort.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        btnFilter.alpha = 1
        btnShort.alpha = 0.5
        viewFilter.isHidden = false
        viewShort.isHidden = true
    }
    
    @IBAction func clickedShort(_ sender: Any) {
        btnFilter.alpha = 0.5
        btnShort.alpha = 1
        viewFilter.isHidden = true
        viewShort.isHidden = false
        
    }
    
    
    
    @IBAction func clickedBackk(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
}
