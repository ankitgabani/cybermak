//
//  AddTicketViewController.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class AddTicketViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
