//
//  HomeViewController.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var imgFeed: UIImageView!
    @IBOutlet weak var imhUser: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewUser: UIView!
    
    @IBOutlet weak var saveTopCont: NSLayoutConstraint!
    @IBOutlet weak var saveCont: NSLayoutConstraint!
    
    var isSaveClick = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        
        viewMenu.isHidden = false
        viewUser.isHidden = true
        saveCont.constant = 0
        saveTopCont.constant = 0
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedMenu(_ sender: Any) {
        imgMenu.image = UIImage(named: "menu_select")
        imgFeed.image = UIImage(named: "feed_unselect")
        imhUser.image = UIImage(named: "profile_unselect")
        viewMenu.isHidden = false
        viewUser.isHidden = true
    }
    
    @IBAction func clickedFeed(_ sender: Any) {
        imgMenu.image = UIImage(named: "menu_unselect")
        imgFeed.image = UIImage(named: "feed_select")
        imhUser.image = UIImage(named: "profile_unselect")
        viewMenu.isHidden = false
        viewUser.isHidden = true
        
    }
    
    @IBAction func clickedUSer(_ sender: Any) {
        imgMenu.image = UIImage(named: "menu_unselect")
        imgFeed.image = UIImage(named: "feed_unselect")
        imhUser.image = UIImage(named: "profile_select")
        viewMenu.isHidden = true
        viewUser.isHidden = false
        
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = homeNavigation
        window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedSaveUnSave(_ sender: Any) {
        
        if isSaveClick == true {
            isSaveClick = false
            saveCont.constant = 0
            saveTopCont.constant = 0
        } else {
            isSaveClick = true
            saveCont.constant = 45
            saveTopCont.constant = 10
        }
    }
    
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        if indexPath.row % 2 == 0{
            
            cell.mainView.backgroundColor = UIColor(red: 19/255, green: 99/255, blue: 183/255, alpha: 1)
            cell.lbl1.textColor = UIColor.white
            cell.lbl2.textColor = UIColor.white
            cell.lblTicketID.textColor = UIColor.white
            cell.lblMSISDN.textColor = UIColor.white
        }else{
            
            cell.mainView.backgroundColor = UIColor.white
            cell.lbl1.textColor = UIColor(red: 19/255, green: 99/255, blue: 183/255, alpha: 1)
            cell.lbl2.textColor = UIColor(red: 19/255, green: 99/255, blue: 183/255, alpha: 1)
            cell.lblTicketID.textColor = UIColor(red: 19/255, green: 99/255, blue: 183/255, alpha: 1)
            cell.lblMSISDN.textColor = UIColor(red: 19/255, green: 99/255, blue: 183/255, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TicketDetailsVC") as! TicketDetailsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension UINavigationController
{
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}
