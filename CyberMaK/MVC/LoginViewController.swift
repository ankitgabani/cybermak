//
//  LoginViewController.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtUserName: UITextField!
    
    @IBOutlet weak var txtPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtUserName.text = "islamcmak"
        txtPass.text = "123"

        // Do any additional setup after loading the view.
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        
        if txtUserName.text == "" {
            self.view.makeToast("please enter your username")
            return false
        } else if txtPass.text == "" {
            self.view.makeToast("please enter your password")
            return false
        }
        
        return true
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        if self.isValidatedReset() {
            callLogin()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- API Call
    func callLogin() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let param = ["username": txtUserName.text!,"password": txtPass.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_LOGIN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(self.txtUserName.text!, forKey: "UserPhone")
                        UserDefaults.standard.set(self.txtPass.text!, forKey: "UserPassword")
                        UserDefaults.standard.synchronize()
                        
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
}
