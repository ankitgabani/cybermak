//
//  TicketDetailsVC.swift
//  CyberMaK
//
//  Created by Ankit on 11/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class TicketDetailsVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewOptionMc: UIView!
    
    
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var viewAssigemnet: UIView!
    @IBOutlet weak var viewQA: UIView!
    @IBOutlet weak var viewHistory: UIView!
    
    @IBOutlet weak var tblViewHistory: UITableView!
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var lblAssignment: UILabel!
    
    @IBOutlet weak var lblQA: UILabel!
    
    @IBOutlet weak var lblHistory: UILabel!
    
    var isOpenOption = false
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDetails.alpha = 1
        lblAssignment.alpha = 0.5
        lblQA.alpha = 0.5
        lblHistory.alpha = 0.5
        
        viewDetails.isHidden = false
        viewAssigemnet.isHidden = true
        viewQA.isHidden = true
        viewHistory.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        isOpenOption = false
        viewBg.isHidden = true
        viewOptionMc.isHidden = true
        
    }
    
    @IBAction func clickedDetails(_ sender: Any) {
        lblDetails.alpha = 1
        lblAssignment.alpha = 0.5
        lblQA.alpha = 0.5
        lblHistory.alpha = 0.5
        
        viewDetails.isHidden = false
        viewAssigemnet.isHidden = true
        viewQA.isHidden = true
        viewHistory.isHidden = true
    }
    
    @IBAction func clickedAss(_ sender: Any) {
        lblDetails.alpha = 0.5
        lblAssignment.alpha = 1
        lblQA.alpha = 0.5
        lblHistory.alpha = 0.5
        
        viewDetails.isHidden = true
        viewAssigemnet.isHidden = false
        viewQA.isHidden = true
        viewHistory.isHidden = true
        
    }
    
    @IBAction func clickedQA(_ sender: Any) {
        lblDetails.alpha = 0.5
        lblAssignment.alpha = 0.5
        lblQA.alpha = 1
        lblHistory.alpha = 0.5
        
        viewDetails.isHidden = true
        viewAssigemnet.isHidden = true
        viewQA.isHidden = false
        viewHistory.isHidden = true
    }
    
    @IBAction func clickedHistory(_ sender: Any) {
        lblDetails.alpha = 0.5
        lblAssignment.alpha = 0.5
        lblQA.alpha = 0.5
        lblHistory.alpha = 1
        
        viewDetails.isHidden = true
        viewAssigemnet.isHidden = true
        viewQA.isHidden = true
        viewHistory.isHidden = false
    }
    
    @IBAction func clickedBottomOption(_ sender: Any) {
        
        if isOpenOption == true {
            isOpenOption = false
            viewBg.isHidden = true
            viewOptionMc.isHidden = true
        } else {
            isOpenOption = true
            viewBg.isHidden = false
            viewOptionMc.isHidden = false
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
}
