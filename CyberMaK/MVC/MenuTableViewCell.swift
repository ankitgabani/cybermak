//
//  MenuTableViewCell.swift
//  CyberMaK
//
//  Created by Ankit on 09/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTicketID: UILabel!
    @IBOutlet weak var lblMSISDN: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
