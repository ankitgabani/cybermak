//
//  Constants.swift
//  CyberMaK
//
//  Created by Ankit on 31/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import Foundation

// Dev
//let BASE_URL = "https://dev-bookist.herokuapp.com/api/"

// Pro
let BASE_URL = "https://168.187.92.239:443/api/"

let USER_LOGIN = "jwt/login"
