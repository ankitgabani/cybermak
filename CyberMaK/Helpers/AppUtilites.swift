//  AppUtilites.swift
//  Blipd
//
//  Created by Harry on 2/26/18.
//  Copyright © 2018 Harry. All rights reserved.
//

import UIKit
import Photos
import CoreLocation

class AppUtilites: NSObject {
    
    class var sharedInstance: AppUtilites {
        struct Static {
            static let instance: AppUtilites = AppUtilites()
        }
        return Static.instance
    }
    
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        func getViewController(controllerName: String) -> UIViewController {
        return mainStoryboard.instantiateViewController(withIdentifier: controllerName) 
    }

  let friendStoryboard: UIStoryboard = UIStoryboard(name: "Friend", bundle: nil)
  func getViewControllerFromFriend(controllerName: String) -> UIViewController {
    return friendStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let groupStoryboard: UIStoryboard = UIStoryboard(name: "Group", bundle: nil)
  func getViewControllerFromGroup(controllerName: String) -> UIViewController {
    return groupStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let eidtProfileStoryboard: UIStoryboard = UIStoryboard(name: "EditProfile", bundle: nil)
  func getViewControllerFromEditProfile(controllerName: String) -> UIViewController {
    return eidtProfileStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let userProfileStoryboard: UIStoryboard = UIStoryboard(name: "UserProfile", bundle: nil)
  func getViewControllerFromUserProfile(controllerName: String) -> UIViewController {
    return userProfileStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let feedViewStoryboard: UIStoryboard = UIStoryboard(name: "FeedView", bundle: nil)
  func getViewControllerFromFeedView(controllerName: String) -> UIViewController {
    return feedViewStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let markedLocationStoryboard: UIStoryboard = UIStoryboard(name: "MarkedLocation", bundle: nil)
  func getViewControllerFromMarkedLocation(controllerName: String) -> UIViewController {
    return markedLocationStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let settingStoryboard: UIStoryboard = UIStoryboard(name: "Setting", bundle: nil)
  func getViewControllerFromSetting(controllerName: String) -> UIViewController {
    return settingStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let accountsStoryboard: UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
  func getViewControllerFromAccounts(controllerName: String) -> UIViewController {
    return accountsStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let notificationStoryboard: UIStoryboard = UIStoryboard(name: "Notification", bundle: nil)
  func getViewControllerFromNotification(controllerName: String) -> UIViewController {
    return notificationStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let addLocationStoryboard: UIStoryboard = UIStoryboard(name: "AddLocation", bundle: nil)
  func getViewControllerFromAddLocation(controllerName: String) -> UIViewController {
    return addLocationStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let gridViewStoryboard: UIStoryboard = UIStoryboard(name: "GridView", bundle: nil)
  func getViewControllerFromGridView(controllerName: String) -> UIViewController {
    return gridViewStoryboard.instantiateViewController(withIdentifier: controllerName)
  }

  let editPostStoryboard: UIStoryboard = UIStoryboard(name: "EditPost", bundle: nil)
  func getViewControllerFromEditPost(controllerName: String) -> UIViewController {
    return editPostStoryboard.instantiateViewController(withIdentifier: controllerName)
  }


    func isValidatePresence(string: String) -> Bool {
        
        let trimmed: String = string.trimmingCharacters(in: CharacterSet.whitespaces)
        return !trimmed.isEmpty
    }
    
    func isValidPostalCode(string: String) -> Bool {
        let emailRegEx = "^[0-9A-Za-z]{5,7}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    func isValidPassword(testStr: String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    
    func isValidEmail(string: String) -> Bool {
        
       // let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
      
//        let emailRegEx = "^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
  
    func isValidPhoneNumber(string: String) -> Bool {
        let PHONE_REGEX = "^\\d{10}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
    
    func isOnlyNumber(string: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
    
    func isValidPhoneNumberWithCode(string: String) -> Bool {
        let PHONE_REGEX = "^\\+?91?\\s*\\(?-*\\.*(\\d{3})\\)?\\.*-*\\s*(\\d{3})\\.*-*\\s*(\\d{4})$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
     
    
    func getBottomLineView() -> UIView{
        let width = UIScreen.main.bounds.width
        let coustomView2 = UIView()
        coustomView2.frame = CGRect(x: 0, y: 44, width: width, height: 1)
        coustomView2.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        
        return coustomView2
        
    }
    
    func getTopLineView() -> UIView
    {
        let width = UIScreen.main.bounds.width
        let coustomView = UIView()
        coustomView.frame = CGRect(x: 0, y: 0, width: width, height: 1)
        coustomView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        return coustomView
    }


  func isTokenExpired() -> Bool
  {
    let expiresat = UserDefaults.standard.value(forKey: "expiresat") as? Int ?? 0
    let timeStampCurrent = Int(Date().timeIntervalSince1970)

    if timeStampCurrent > expiresat
    {
      return true
    } else {
      return false
    }

  }
  
}
